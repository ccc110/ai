import numpy as np

class Node:
    def __init__(self, shape):
        if not isinstance(shape, tuple):
            shape = (shape,) # Note the comma
        self.shape = shape
        self.v = np.zeros(shape)
        self.g = np.zeros(shape)

    def zero_grad(self):
        self.g.fill(0)

    def random(self):
        self.v = np.random.rand(*self.shape)

class Linear:
    def __init__(self, ni, no):
        self.W = Node((ni, no))
        self.b = Node((no,))
        self.W.random()
        self.b.random()

    def forward(self, x):
        return x.dot(self.W.v) + self.b.v

    def parameters(self):
        return [self.W, self.b]

    def zero_grad(self):
        self.W.zero_grad()
        self.b.zero_grad()

# https://optimization.cbe.cornell.edu/index.php?title=RMSProp#RMSProp
class MSELoss:
    def __init__(self):
        pass
        
    def forward(self, outputs, targets):
        d = outputs-targets
        print('outputs=', outputs, 'targets=', targets, 'd=', d)
        # print('d.dot(d)=', d.dot(d))
        print('MSELoss(d)=', (d**2).sum().item())
        self.d = d
        return (d**2).sum().item()
        # return d.dot(d)

    def backward(self):
        g = 2.0 * self.d
        print()
        return d.dot(d)

class Net:
    def __init__ (self):
        self.layers = []

    def addLayer(self, layer):
        self.layers.append(layer)

    def forward(self, x): # 正向傳遞計算結果
        r = x
        for layer in self.layers:
            r = layer.forward(r)
            print('r=', r)

    def parameters(self):
        params = []
        for layer in self.layers:
            params.extend(layer.parameters())
        return params

class GD:
    def __init__ (self, parameters, lr):
        self.parameters = parameters
        self.lr = lr

    def zero_grad(self):
        for node in parameters:
            node.zero_grad()

