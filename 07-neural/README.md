
## Pytorch

* https://pytorch.org/hub/research-models

* https://github.com/INTERMT/Awesome-PyTorch-Chinese (讚)

* [Medium: 李謦伊](https://medium.com/@chingi071)

## neural

* https://github.com/SkalskiP/ILearnDeepLearning.py (讚)
    * https://github.com/SkalskiP/ILearnDeepLearning.py/blob/master/01_mysteries_of_neural_networks/03_numpy_neural_net/
    * https://towardsdatascience.com/lets-code-convolutional-neural-network-in-plain-numpy-ce48e732f5d5
* [Neural Networks Backpropagation Made Easy](https://towardsdatascience.com/neural-networks-backpropagation-by-dr-lihi-gur-arie-27be67d8fdce)
* [資料科學・機器・人](https://brohrer.mcknote.com/zh-Hant/)